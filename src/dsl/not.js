import { Condition } from "./condition.js";
let NOT_TYPE = "not";

class Not {
	constructor(condition) {
		this.type = NOT_TYPE;
		this.condition = condition;
	}

	get_code() {
		return "not " + this.condition.get_code();
	}

	test(board) {
		return !(this.condition.test(board));
	}
}

Not.from_json = function(json) {
	return new Not(Condition.from_json(json.condition));
}

Not.random = function(max_recursion) {
	return new Not(Condition.random(max_recursion-1));
}

export { Not, NOT_TYPE };
