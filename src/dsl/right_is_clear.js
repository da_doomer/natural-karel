import { RIGHT, LEFT, UP, DOWN } from "../board.js";
let RIGHT_IS_CLEAR_TYPE = "right_is_clear";

class RightIsClear {
	constructor() {
		this.type = RIGHT_IS_CLEAR_TYPE;
	}

	get_code() {
		return "left_is_clear()";
	}

	test(board) {
		let i = board.get_robot_position()[0];
		let j = board.get_robot_position()[1];
		let new_i = i;
		let new_j = j;
		if(board.get_robot_orientation() == UP)
			new_i += 1;
		if(board.get_robot_orientation() == DOWN)
			new_i -= 1;
		if(board.get_robot_orientation() == RIGHT)
			new_j -= 1;
		if(board.get_robot_orientation() == LEFT)
			new_j += 1;
		if(!board.is_wall([new_i, new_j]))
			return true;
		return false;
	}
}

RightIsClear.from_json = function(json) {
	return new RightIsClear();
}

RightIsClear.random = function(max_recursion) {
	return new RightIsClear();
}

export { RightIsClear, RIGHT_IS_CLEAR_TYPE };
