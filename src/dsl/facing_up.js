import { UP } from "../board.js";

let FACING_UP_TYPE = "facing_up";

class FacingUp {
	constructor() {
		this.type = FACING_UP_TYPE;
	}

	get_code() {
		return "facing_up()";
	}

	test(board) {
		if(board.get_robot_orientation() == UP)
			return true;
		return false;
	}
}

FacingUp.from_json = function(json) {
	return new FacingUp();
}

FacingUp.random = function(max_recursion) {
	return new FacingUp();
}

export { FacingUp, FACING_UP_TYPE };
