import { INDENT_SIZE } from "./dsl.js";
import { indent } from "./dsl.js";
import { Constant } from "./constant.js";
import { Statement } from "./statement.js";

let REPEAT_TYPE = "repeat";

class Repeat {
	constructor(constant, statement) {
		this.type = REPEAT_TYPE;
		this.constant = constant;
		this.statement = statement;
	}

	get_code() {
		return "for _ in range(" + this.constant.get_code() + "):\n" + indent(this.statement.get_code(), INDENT_SIZE);
	}

	transform(board, parameters, callback) {
		callback(this);
		for(let i = 0; i < this.constant; i++) {
			if(parameters.max_budget <= 0) break;
			this.statement.transform(board, parameters, callback);
		}
	}
}

Repeat.from_json = function(json) {
	return new Repeat(Constant.from_json(json.constant), Statement.from_json(json.statement));
}

Repeat.random = function(max_recursion) {
	return new Repeat(Constant.random(max_recursion-1), Statement.random(max_recursion-1));
}

export { Repeat, REPEAT_TYPE };
