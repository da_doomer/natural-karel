import { SPECIAL_AREA } from "../board.js";

let IN_SPECIAL_AREA_TYPE = "in_special_area";

class InSpecialArea {
	constructor() {
		this.type = IN_SPECIAL_AREA_TYPE;
	}

	get_code() {
		return "in_special_area()";
	}

	test(board) {
		if(board.get_area_type_at(board.get_robot_position()) == SPECIAL_AREA)
			return true;
		return false;
	}
}

InSpecialArea.from_json = function(json) {
	return new InSpecialArea();
}

InSpecialArea.random = function(max_recursion) {
	return new InSpecialArea();
}

export { InSpecialArea, IN_SPECIAL_AREA_TYPE };
