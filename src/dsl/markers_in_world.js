let MARKERS_IN_WORLD_TYPE = "markers_in_world";

class MarkersInWorld {
	constructor() {
		this.type = MARKERS_IN_WORLD_TYPE;
	}

	get_code() {
		return "markers_in_world()";
	}

	test(board) {
		if(board.get_marker_n() > 0)
			return true;
		return false;
	}
}

MarkersInWorld.from_json = function(json) {
	return new MarkersInWorld();
}

MarkersInWorld.random = function(max_recursion) {
	return new MarkersInWorld();
}

export { MarkersInWorld, MARKERS_IN_WORLD_TYPE };
