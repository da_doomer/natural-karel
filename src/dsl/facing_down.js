import { DOWN } from "../board.js";

let FACING_DOWN_TYPE = "facing_up";

class FacingDown {
	constructor() {
		this.type = FACING_DOWN_TYPE;
	}

	get_code() {
		return "facing_down()";
	}

	test(board) {
		if(board.get_robot_orientation() == DOWN)
			return true;
		return false;
	}
}

FacingDown.from_json = function(json) {
	return new FacingDown();
}

FacingDown.random = function(max_recursion) {
	return new FacingDown();
}

export { FacingDown, FACING_DOWN_TYPE };
