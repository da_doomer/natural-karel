import { Statement } from "./statement.js";

let SEQUENCE_TYPE = "sequence";

class Sequence {
	constructor(statement1, statement2) {
		this.type = SEQUENCE_TYPE;
		this.statement1 = statement1;
		this.statement2 = statement2;
	}

	get_code() {
		return this.statement1.get_code() + "\n" + this.statement2.get_code();
	}

	transform(board, parameters, callback) {
		callback(this);
		this.statement1.transform(board, parameters, callback);
		this.statement2.transform(board, parameters, callback);
	}
}

Sequence.from_json = function(json) {
	return new Sequence(Statement.from_json(json.statement1), Statement.from_json(json.statement2));
}

Sequence.random = function(max_recursion) {
	return new Sequence(Statement.random(max_recursion-1), Statement.random(max_recursion-1));
}

Sequence.from_list = function(statements) {
	if(statements.length == 2)
		return new Sequence(statements[0], statements[1]);
	let tail = [];
	for(let i = 1; i < statements.length; i++)
		tail = [...tail, statements[i]];
	let tail_sequence = Sequence.from_list(tail);
	return new Sequence(statements[0], tail_sequence);
}

export { Sequence, SEQUENCE_TYPE };
