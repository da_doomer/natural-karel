import { RIGHT, LEFT, UP, DOWN } from "../board.js";
let TURN_LEFT_TYPE = "turn_left";

class TurnLeft {
	constructor() {
		this.type = TURN_LEFT_TYPE;
	}

	get_code() {
		return "turn_left()";
	}

	transform(board, parameters, callback) {
		callback(this);
		if(parameters.max_budget <= 0)
			return;
		if(board.get_robot_orientation() == UP)
			board.set_robot_orientation(LEFT);
		else if(board.get_robot_orientation() == DOWN)
			board.set_robot_orientation(RIGHT);
		else if(board.get_robot_orientation() == RIGHT)
			board.set_robot_orientation(UP);
		else if(board.get_robot_orientation() == LEFT)
			board.set_robot_orientation(DOWN);
		parameters.max_budget -= 1;
	}
}

TurnLeft.from_json = function(json) {
	return new TurnLeft();
}

TurnLeft.random = function(max_recursion) {
	return new TurnLeft();
}

export { TurnLeft, TURN_LEFT_TYPE };
