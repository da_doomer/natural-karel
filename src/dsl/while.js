import { INDENT_SIZE } from "./dsl.js";
import { indent } from "./dsl.js";
import { Condition } from "./condition.js";
import { Statement } from "./statement.js";

let WHILE_TYPE = "while";

class While {
	constructor(condition, statement) {
		this.type = WHILE_TYPE;
		this.condition = condition;
		this.statement = statement;
	}

	get_code() {
		return "while " + this.condition.get_code() + ":\n" + indent(this.statement.get_code(), INDENT_SIZE);
	}

	transform(board, parameters, callback) {
		callback(this);
		while(this.condition.test(board)) {
			if(parameters.max_budget <= 0) break;
			this.statement.transform(board, parameters, callback);
			parameters.max_budget -= 1;
		}
	}
}

While.from_json = function(json) {
	return new While(
		Condition.from_json(json.condition),
		Statement.from_json(json.statement)
	);
}

While.random = function(max_recursion) {
	return new While(
		Condition.random(max_recursion-1),
		Statement.random(max_recursion-1)
	);
}

export { While, WHILE_TYPE }
