import { random_item } from "./dsl.js";
import { Move } from "./move.js";
import { MOVE_TYPE } from "./move.js";
import { TurnRight } from "./turn_right.js";
import { TURN_RIGHT_TYPE } from "./turn_right.js";
import { TurnLeft } from "./turn_left.js";
import { TURN_LEFT_TYPE } from "./turn_left.js";
import { PickMarker } from "./pick_marker.js";
import { PICK_MARKER_TYPE } from "./pick_marker.js";
import { PutMarker } from "./put_marker.js";
import { PUT_MARKER_TYPE } from "./put_marker.js";
import { GoToSpecialArea } from "./go_to_special_area.js";
import { GO_TO_SPECIAL_AREA_TYPE } from "./go_to_special_area.js";


const ACTION_TYPES = [
	MOVE_TYPE,
	TURN_RIGHT_TYPE,
	TURN_LEFT_TYPE,
	PICK_MARKER_TYPE,
	PUT_MARKER_TYPE,
	GO_TO_SPECIAL_AREA_TYPE
];

class Action {}

Action.from_json = function(json) {
	if(json.type == MOVE_TYPE)
		return Move.from_json(json);
	if(json.type == TURN_RIGHT_TYPE)
		return TurnRight.from_json(json);
	if(json.type == TURN_LEFT_TYPE)
		return TurnLeft.from_json(json);
	if(json.type == PICK_MARKER_TYPE)
		return PickMarker.from_json(json);
	if(json.type == PUT_MARKER_TYPE)
		return PutMarker.from_json(json);
	if(json.type == GO_TO_SPECIAL_AREA_TYPE)
		return GoToSpecialArea.from_json(json);
}

Action.random = function(max_recursion) {
	return random_item(action_factories).random(max_recursion-1);
}

const action_factories = [
	Move,
	TurnLeft,
	TurnRight,
	PutMarker,
	PickMarker,
	GoToSpecialArea
];

export { Action, ACTION_TYPES, action_factories };
