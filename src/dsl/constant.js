import { get_random_int } from "./dsl.js";
import { CONSTANT_MAX_VALUE } from "./dsl.js";

let CONSTANT_TYPE = "constant";

class Constant {
	constructor(value) {
		this.type = CONSTANT_TYPE;
		this.value = value;
	}

	get_code() {
		return this.value.toString();
	}
}

Constant.from_json = function(json) {
	return new Constant(json.value);
}

Constant.random = function(max_recursion) {
	return new Constant(get_random_int(CONSTANT_MAX_VALUE+1));
}

export { Constant, CONSTANT_TYPE };
