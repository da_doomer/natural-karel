export let INDENT_SIZE = 2;
export let CONSTANT_MAX_VALUE = 10;

function get_random_int(max) {
	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
	return Math.floor(Math.random() * max);
}

function random_item(array) {
	// https://stackoverflow.com/questions/5915096/get-a-random-item-from-a-javascript-array
	return array[Math.floor(Math.random()*array.length)];
}

function indent(string, n) {
	let new_string = "";
	let lines = string.split("\n");
	for(let j = 0; j < lines.length; j++) {
		if(lines[j].length == 0)
			continue;
		let indent = "";
		for(let i = 0; i < n; i++)
			indent = indent + " ";
		new_string = new_string + indent + lines[j];
		new_string = new_string + "\n";
	}
	return new_string;
}

export { random_item, indent, get_random_int };
