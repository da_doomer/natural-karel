let PICK_MARKER_TYPE = "pick_closest_marker";

class PickMarker {
	constructor() {
		this.type = PICK_MARKER_TYPE;
	}

	get_code() {
		return "pick_closest_marker()";
	}

	transform(board, parameters, callback) {
		callback(this);
		if(parameters.max_budget <= 0)
			return;
		// If there is a marker at the position of the robot,
		// pick it up.
		// Else, if there is a marker in the board, do the next step
		// in the shortest path to the nearest marker.
		// Else, do nothing.
		if(board.get_marker_n_at(board.get_robot_position()) > 0) {
			board.pick_marker_at(board.get_robot_position());
			board.set_robot_marker_n(board.get_robot_marker_n()+1);
		} else if(board.get_marker_n() > 0) {
			let closest_marker_position = board.closest_marker_position_to(
				board.get_robot_position()
			);
			if(closest_marker_position !== null) {
				let actions = board.shortest_actions_to(closest_marker_position);
				let action = actions[0];
				action.transform(board, parameters, callback);
			}
		}
		parameters.max_budget -= 1;
	}
}

PickMarker.from_json = function(json) {
	return new PickMarker();
}

PickMarker.random = function(max_recursion) {
	return new PickMarker();
}

export { PickMarker, PICK_MARKER_TYPE };
