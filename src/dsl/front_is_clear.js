import { RIGHT, LEFT, UP, DOWN } from "../board.js";
let FRONT_IS_CLEAR_TYPE = "front_is_clear";

class FrontIsClear {
	constructor() {
		this.type = FRONT_IS_CLEAR_TYPE;
	}

	get_code() {
		return "front_is_clear()";
	}

	test(board) {
		let i = board.get_robot_position()[0];
		let j = board.get_robot_position()[1];
		let new_i = i;
		let new_j = j;
		if(board.get_robot_orientation() == UP)
			new_j += 1;
		if(board.get_robot_orientation() == DOWN)
			new_j -= 1;
		if(board.get_robot_orientation() == RIGHT)
			new_i += 1;
		if(board.get_robot_orientation() == LEFT)
			new_i -= 1;
		if(!(board.is_wall([new_i, new_j])))
			return true;
		return false;
	}
}

FrontIsClear.from_json = function(json) {
	return new FrontIsClear();
}

FrontIsClear.random = function(max_recursion) {
	return new FrontIsClear();
}

export { FrontIsClear, FRONT_IS_CLEAR_TYPE };
