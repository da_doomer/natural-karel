import { FrontIsClear } from "./front_is_clear.js";
import { FRONT_IS_CLEAR_TYPE } from "./front_is_clear.js";
import { LeftIsClear } from "./left_is_clear.js";
import { LEFT_IS_CLEAR_TYPE } from "./left_is_clear.js";
import { RightIsClear } from "./right_is_clear.js";
import { RIGHT_IS_CLEAR_TYPE } from "./right_is_clear.js";
import { MarkersPresent } from "./markers_present.js";
import { MARKERS_PRESENT_TYPE } from "./markers_present.js";
import { NoMarkersPresent } from "./no_markers_present.js";
import { NO_MARKERS_PRESENT_TYPE } from "./no_markers_present.js";
import { MarkersInWorld } from "./markers_in_world.js";
import { MARKERS_IN_WORLD_TYPE } from "./markers_in_world.js";
import { Not } from "./not.js";
import { NOT_TYPE } from "./not.js";
import { FacingUp } from "./facing_up.js";
import { FACING_UP_TYPE } from "./facing_up.js";
import { FacingDown } from "./facing_down.js";
import { FACING_DOWN_TYPE } from "./facing_down.js";
import { FacingLeft } from "./facing_left.js";
import { FACING_LEFT_TYPE } from "./facing_left.js";
import { FacingRight } from "./facing_right.js";
import { FACING_RIGHT_TYPE } from "./facing_right.js";
import { InSpecialArea } from "./in_special_area.js";
import { IN_SPECIAL_AREA_TYPE } from "./in_special_area.js";
import { random_item } from "./dsl.js";

class Condition {}

Condition.from_json = function(json) {
	if(json.type == FRONT_IS_CLEAR_TYPE)
		return new FrontIsClear.from_json(json);
	if(json.type == LEFT_IS_CLEAR_TYPE)
		return new LeftIsClear.from_json(json);
	if(json.type == RIGHT_IS_CLEAR_TYPE)
		return new RightIsClear.from_json(json);
	if(json.type == MARKERS_PRESENT_TYPE)
		return new MarkersPresent.from_json(json);
	if(json.type == NO_MARKERS_PRESENT_TYPE)
		return new NoMarkersPresent.from_json(json);
	if(json.type == MARKERS_IN_WORLD_TYPE)
		return new MarkersInWorld.from_json(json);
	if(json.type == NOT_TYPE)
		return new Not.from_json(json);
	if(json.type == FACING_UP_TYPE)
		return new FacingUp.from_json(json);
	if(json.type == FACING_DOWN_TYPE)
		return new FacingDown.from_json(json);
	if(json.type == IN_SPECIAL_AREA_TYPE)
		return new InSpecialArea.from_json(json);
}

Condition.random = function(max_recursion) {
	let classes = condition_factories;
	let Class = random_item(classes);
	return Class.random(max_recursion-1);
}

const condition_factories = [
	FrontIsClear,
	LeftIsClear,
	RightIsClear,
	MarkersPresent,
	NoMarkersPresent,
	MarkersInWorld,
	Not,
	FacingUp,
	FacingDown,
	FacingLeft,
	FacingRight,
	InSpecialArea,
];

// Conditions that require no arguments (i.e. without Not)
const simple_condition_factories = [
	FrontIsClear,
	LeftIsClear,
	RightIsClear,
	MarkersPresent,
	NoMarkersPresent,
	MarkersInWorld,
	FacingUp,
	FacingDown,
	FacingLeft,
	FacingRight,
	InSpecialArea,
];

export {
	Condition,
	condition_factories,
	simple_condition_factories
};
