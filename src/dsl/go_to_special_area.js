import { SPECIAL_AREA } from "../board.js";
let GO_TO_SPECIAL_AREA_TYPE = "go_to_special_area";

class GoToSpecialArea {
	constructor() {
		this.type = GO_TO_SPECIAL_AREA_TYPE;
	}

	get_code() {
		return "go_to_special_area()";
	}

	transform(board, parameters, callback) {
		callback(this);
		if(parameters.max_budget <= 0)
			return;
		// If the robot is not in a special area and there exist special areas on
		// the board, go the next step in the shortest path to the nearest special
		// area.
		// Else, do nothing.
		if(
			board.get_special_area_n() > 0
			&& board.get_area_type_at(board.get_robot_position()) != SPECIAL_AREA
		) {
			let closest_special_area_position = board.closest_special_area_position_to(
				board.get_robot_position()
			);
			if(closest_special_area_position !== null) {
				let actions = board.shortest_actions_to(closest_special_area_position);
				let action = actions[0];
				action.transform(board, parameters, callback);
			}
		}
		parameters.max_budget -= 1;
	}
}

GoToSpecialArea.from_json = function(json) {
	return new GoToSpecialArea();
}

GoToSpecialArea.random = function(max_recursion) {
	return new GoToSpecialArea();
}

export { GoToSpecialArea, GO_TO_SPECIAL_AREA_TYPE };
