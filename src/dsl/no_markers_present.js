let NO_MARKERS_PRESENT_TYPE = "no_markers_present";

class NoMarkersPresent {
	constructor() {
		this.type = NO_MARKERS_PRESENT_TYPE;
	}

	get_code() {
		return "no_markers_present()";
	}

	test(board) {
		if(board.get_marker_n_at(board.get_robot_position()))
			return false;
		return true;
	}
}

NoMarkersPresent.from_json = function(json) {
	return new NoMarkersPresent();
}

NoMarkersPresent.random = function(max_recursion) {
	return new NoMarkersPresent();
}

export { NoMarkersPresent, NO_MARKERS_PRESENT_TYPE };
