import { LEFT } from "../board.js";

let FACING_LEFT_TYPE = "facing_up";

class FacingLeft {
	constructor() {
		this.type = FACING_LEFT_TYPE;
	}

	get_code() {
		return "facing_left()";
	}

	test(board) {
		if(board.get_robot_orientation() == LEFT)
			return true;
		return false;
	}
}

FacingLeft.from_json = function(json) {
	return new FacingLeft();
}

FacingLeft.random = function(max_recursion) {
	return new FacingLeft();
}

export { FacingLeft, FACING_LEFT_TYPE };
