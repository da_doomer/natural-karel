import { INDENT_SIZE } from "./dsl.js";
import { indent } from "./dsl.js";
import { Statement } from "./statement.js";

let PROGRAM_TYPE = "program";

class Program {
	constructor(statement, name="run") {
		this.type = PROGRAM_TYPE;
		this.statement = statement;
		this.name = name;
	}

	get_code() {
		return this.name + ":\n" + indent(this.statement.get_code(), INDENT_SIZE);
	}

	transform(board, parameters, callback) {
		callback(this);
		this.statement.transform(board, parameters, callback);
	};
}

Program.from_json = function(json) {
	return new Program(json.statement);
}

Program.random = function(max_recursion) {
	return new Program(Statement.random(max_recursion-1));
}

export { Program, PROGRAM_TYPE };
