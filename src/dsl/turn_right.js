import { RIGHT, LEFT, UP, DOWN } from "../board.js";
let TURN_RIGHT_TYPE = "turn_right";

class TurnRight {
	constructor() {
		this.type = TURN_RIGHT_TYPE;
	}

	get_code() {
		return "turn_right()";
	}

	transform(board, parameters, callback) {
		callback(this);
		if(parameters.max_budget <= 0)
			return;
		if(board.get_robot_orientation() == UP)
			board.set_robot_orientation(RIGHT);
		else if(board.get_robot_orientation() == DOWN)
			board.set_robot_orientation(LEFT);
		else if(board.get_robot_orientation() == RIGHT)
			board.set_robot_orientation(DOWN);
		else if(board.get_robot_orientation() == LEFT)
			board.set_robot_orientation(UP);
		parameters.max_budget -= 1;
	}
}

TurnRight.from_json = function(json) {
	return new TurnRight();
}

TurnRight.random = function(max_recursion) {
	return new TurnRight();
}

export { TurnRight, TURN_RIGHT_TYPE };
