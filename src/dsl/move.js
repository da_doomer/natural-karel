import { RIGHT, LEFT, UP, DOWN } from "../board.js";
let MOVE_TYPE = "move";

class Move {
	constructor() {
		this.type = MOVE_TYPE;
	}

	get_code() {
		return "move()";
	}

	transform(board, parameters, callback) {
		callback(this);
		if(parameters.max_budget <= 0)
			return;
		let i = board.get_robot_position()[0];
		let j = board.get_robot_position()[1];
		let new_i = i;
		let new_j = j;
		if(board.get_robot_orientation() == UP)
			new_j += 1;
		if(board.get_robot_orientation() == DOWN)
			new_j -= 1;
		if(board.get_robot_orientation() == RIGHT)
			new_i += 1;
		if(board.get_robot_orientation() == LEFT)
			new_i -= 1;
		if(!board.is_wall([new_i, new_j]))
			board.set_robot_position([new_i, new_j]);
		parameters.max_budget -= 1;
	}
}

Move.from_json = function(json) {
	return new Move();
}

Move.random = function(max_recursion) {
	return new Move();
}

export { Move, MOVE_TYPE };
