import { INDENT_SIZE } from "./dsl.js";
import { indent } from "./dsl.js";
import { Condition } from "./condition.js";
import { Statement } from "./statement.js";

let IF_TYPE = "if";

class If {
	constructor(condition, statement) {
		this.type = IF_TYPE;
		this.condition = condition;
		this.statement = statement;
	}

	get_code() {
		return "if " + this.condition.get_code() + ":\n" + indent(this.statement.get_code(), INDENT_SIZE);
	}

	transform(board, parameters, callback) {
		callback(this);
		if(this.condition.test(board))
			this.statement.transform(board, parameters, callback);
	}
}

If.from_json = function(json) {
	return new If(Condition.from_json(json.condition), Statement.from_json(json.statement));
}

If.random = function(max_recursion) {
	return new If(Condition.random(max_recursion-1), Statement.random(max_recursion-1));
}

export { If, IF_TYPE };
