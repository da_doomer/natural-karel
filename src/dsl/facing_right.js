import { RIGHT } from "../board.js";

let FACING_RIGHT_TYPE = "facing_up";

class FacingRight {
	constructor() {
		this.type = FACING_RIGHT_TYPE;
	}

	get_code() {
		return "facing_right()";
	}

	test(board) {
		if(board.get_robot_orientation() == RIGHT)
			return true;
		return false;
	}
}

FacingRight.from_json = function(json) {
	return new FacingRight();
}

FacingRight.random = function(max_recursion) {
	return new FacingRight();
}

export { FacingRight, FACING_RIGHT_TYPE };
