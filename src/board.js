/**
 * Karel board.
 */
import { TurnLeft } from "./dsl/turn_left.js";
import { TurnRight } from "./dsl/turn_right.js";
import { Move } from "./dsl/move.js";
import { all_pairs_shortest_paths } from "./graph.js";


const RIGHT = "right";
const LEFT = "left";
const UP = "up";
const DOWN = "down";

const NORMAL_AREA = "normal_area";
const SPECIAL_AREA = "special_area";

function are_adjacent_coordinates([i1, j1], [i2, j2]) {
	let i_delta = Math.abs(i1 - i2);
	let j_delta = Math.abs(j1 - j2);
	if(i_delta + j_delta == 1)
		return true;
	return false;
}


/**
 * Return a list of actions that change the source orientation to
 * the target orientation.
 */
function heading_actions(orientation1, orientation2) {
	// This is not the shortest code possible, but is simple enough
	if(orientation1 == orientation2)
		return [];
	if(
		orientation1 == UP && orientation2 == DOWN
		|| orientation1 == DOWN && orientation2 == UP
		|| orientation1 == LEFT && orientation2 == RIGHT
		|| orientation1 == RIGHT && orientation2 == LEFT
	)
		return [new TurnLeft(), new TurnLeft()];

	if(
		orientation1 == RIGHT && orientation2 == UP
		|| orientation1 == UP && orientation2 == LEFT
		|| orientation1 == LEFT && orientation2 == DOWN
		|| orientation1 == DOWN && orientation2 == RIGHT
	)
		return [new TurnLeft()];
	return [new TurnRight()];
}


/**
 * Return the relative cardinal position of the second coordinate
 * with respect to the first coordinate (i.e. UP, DOWN, LEFT or RIGHT).
 *
 * The coordinates are assumed to be adjacent.
 *
 * If the coordinates are equal, then behaviour is undefined.
 */
function cardinal_relative_position([i1, j1], [i2, j2]) {
	if(i1 > i2)
		return LEFT;
	if(i1 < i2)
		return RIGHT;
	if(j1 < j2)
		return UP;
	return DOWN;
}

class Board {
	/*
	 * A Karel Board with a robot position and a robot orientation, a dictionary
	 * mapping coordinates to marker numbers and a dictionary mapping coordinates
	 * to booleans representing whether the space is occupied by a wall.
	 *
	 * In this implementation walls occupy a whole block.
	 *
	 * Additionally, blocks may be labeled as "special areas".
	 */
	constructor() {
		this.marker_n_at = {};
		this.wall_at = {};
		this.area_type_at = {};
		this.robot_marker_n = 0;
		this.robot_position = [0, 0];
		this.robot_orientation = RIGHT;
		this.touched_is = [this.robot_position[0]];
		this.touched_js = [this.robot_position[1]];
	}

	get_robot_position() {
		return this.robot_position;
	}

	set_robot_position([i, j]) {
		this.robot_position = [i, j];
		this.touched_is.push(i);
		this.touched_js.push(j);
	}

	get_robot_orientation(orientation) {
		return this.robot_orientation;
	}

	set_robot_orientation(orientation) {
		this.robot_orientation = orientation;
	}

	get_robot_marker_n() {
		return this.robot_marker_n;
	}

	set_robot_marker_n(n) {
		this.robot_marker_n = n;
	}

	/**
	 * Returns the number of markers in the entire board.
	 */
	get_marker_n() {
		let total = 0;
		for(let c of this.get_active_coordinates())
			total += this.get_marker_n_at(c);
		return total;
	}

	get_marker_n_at([i, j]) {
		if([i, j] in this.marker_n_at)
			return this.marker_n_at[[i, j]];
		return 0;
	}

	put_marker_at([i, j]) {
		if([i, j] in this.marker_n_at)
			this.marker_n_at[[i, j]] += 1;
		else
			this.marker_n_at[[i, j]] = 1;
		this.touched_is.push(i);
		this.touched_js.push(j);
	}

	pick_marker_at([i, j]) {
		if([i, j] in this.marker_n_at && this.marker_n_at[[i,j]] > 0)
			this.marker_n_at[[i, j]] -= 1;
		this.touched_is.push(i);
		this.touched_js.push(j);
	}

	is_wall([i, j]) {
		if([i, j] in this.wall_at)
			return this.wall_at[[i, j]];
		return false;
	}

	set_wall([i, j], value) {
		this.wall_at[[i, j]] = value;
		this.touched_is.push(i);
		this.touched_js.push(j);
	}

	get_area_type_at([i, j]) {
		if([i, j] in this.area_type_at)
			return this.area_type_at[[i,j]];
		return NORMAL_AREA;
	}

	set_area_type_at([i, j], area_type) {
		this.area_type_at[[i, j]] = area_type;
		this.touched_is.push(i);
		this.touched_js.push(j);
	}

	get_special_area_n() {
		let special_area_n = 0;
		for(let c of this.get_active_coordinates())
			if(this.get_area_type_at(c) == SPECIAL_AREA)
				special_area_n += 1;
		return special_area_n;
	}

	get_min_i() {
		return Math.min(...this.touched_is);
	}

	get_max_i() {
		return Math.max(...this.touched_is);
	}

	get_min_j() {
		return Math.min(...this.touched_js);
	}

	get_max_j() {
		return Math.max(...this.touched_js);
	}

	get_active_coordinates() {
		let active_coordinates = [];
		let min_i = this.get_min_i();
		let max_i = this.get_max_i();
		let min_j = this.get_min_j();
		let max_j = this.get_max_j();
		for(let i = min_i; i <= max_i; i++)
		for(let j = min_j; j <= max_j; j++) {
			active_coordinates = [...active_coordinates, [i, j]];
		}
		return active_coordinates;
	}

	are_adjacent([i1, j1], [i2, j2]) {
		// If they are not adjacent as coordinates, then they are not adjacent
		if(!are_adjacent_coordinates([i1, j1], [i2, j2]))
			return false;

		// If any of them is a wall, then they are not adjacent
		if(this.is_wall([i1, j1]) || this.is_wall([i2, j2]))
			return false;

		// Else they are adjacent
		return true;
	}

	/**
	 * Returns a list with the coordinates of all active vertices in the board.
	 */
	get_vertex_coordinates() {
		let vertex_coordinates = [];
		for(let i = this.get_min_i(); i <= this.get_max_i(); i++)
		for(let j = this.get_min_j(); j <= this.get_max_j(); j++) {
			vertex_coordinates = [...vertex_coordinates, [i, j]];
		}
		return vertex_coordinates;
	}

	/**
	 * Create an adjacency matrix using the given list of vertex coordinates
	 * as the vertex enumeration.
	 */
	get_adjacency_matrix(vertex_coordinates) {
		// Create the adjacency matrix with marker-including vertices
		// labeled with 1, and an edge for all adjacent vertices
		let vertex_n = vertex_coordinates.length;
		let adjacency_matrix = Array(vertex_n).fill().map(() => Array(vertex_n).fill());
		for(let i = 0; i < vertex_n; i++)
		for(let j = 0; j < vertex_n; j++) {
			if(i === j) {
				// This is a vertex
				adjacency_matrix[i][i] = 0;
			} else {
				// This may be an edge
				let edge_weight = Infinity;
				let v1 = vertex_coordinates[i];
				let v2 = vertex_coordinates[j];
				let are_adjacent = this.are_adjacent(v1, v2);
				if(are_adjacent)
					edge_weight = 1;
				adjacency_matrix[i][j] = edge_weight;
			}
		}
		return adjacency_matrix;
	}

	/**
	 * Returns the coordinates of the closest coordinate with markers.
	 * If there are no markers on the board, behaviour is undefined.
	 */
	closest_marker_position_to([ci, cj]) {
		// Add the given coordinate to the active board
		this.touched_is.push(ci);
		this.touched_js.push(cj);

		// Identify the index of the given coordinate in the
		// vertex enumeration
		let vertex_coordinates = this.get_vertex_coordinates();
		let vertex_n = vertex_coordinates.length;
		let source_vertex = 0;
		for(let i = 0; i < vertex_n; i++) {
			let [vi, vj] = vertex_coordinates[i];
			if(vi == ci && vj == cj)
				source_vertex = i;
		}

		// Find the closest marker-including vertex
		let adjacency_matrix = this.get_adjacency_matrix(vertex_coordinates);
		let [shortest_distances, shortest_paths] = all_pairs_shortest_paths(
			adjacency_matrix
		);
		let closest_vertex = 0;
		let shortest_distance = Infinity;
		for(let i = 0; i < vertex_n; i++) {
			if(i == source_vertex)
				continue;
			let d = shortest_distances[i][source_vertex];
			if(
				d < shortest_distance
				&& this.get_marker_n_at(vertex_coordinates[i]) > 0
			) {
				shortest_distance = d;
				closest_vertex = i;
			}
		}

		// Return the coordinates of the closest vertex
		return vertex_coordinates[closest_vertex];
	}

	closest_special_area_position_to([ci, cj]) {
		// Add the given coordinate to the active board
		this.touched_is.push(ci);
		this.touched_js.push(cj);

		// Identify the index of the given coordinate in the
		// vertex enumeration
		let vertex_coordinates = this.get_vertex_coordinates();
		let vertex_n = vertex_coordinates.length;
		let source_vertex = 0;
		for(let i = 0; i < vertex_n; i++) {
			let [vi, vj] = vertex_coordinates[i];
			if(vi == ci && vj == cj)
				source_vertex = i;
		}

		// Find the closest special-area vertex
		let adjacency_matrix = this.get_adjacency_matrix(vertex_coordinates);
		let [shortest_distances, shortest_paths] = all_pairs_shortest_paths(
			adjacency_matrix
		);
		let closest_vertex = 0;
		let shortest_distance = Infinity;
		for(let i = 0; i < vertex_n; i++) {
			if(i == source_vertex)
				continue;
			let d = shortest_distances[i][source_vertex];
			if(
				d < shortest_distance
				&& this.get_area_type_at(vertex_coordinates[i]) == SPECIAL_AREA
			) {
				shortest_distance = d;
				closest_vertex = i;
			}
		}

		// Return the coordinates of the closest vertex
		return vertex_coordinates[closest_vertex];

	}

	shortest_actions_to([ti, tj]) {
		// Add the given coordinate to the active board
		this.touched_is.push(ti);
		this.touched_js.push(tj);

		// Identify the shortest paths
		let vertex_coordinates = this.get_vertex_coordinates();
		let adjacency_matrix = this.get_adjacency_matrix(vertex_coordinates);
		let [shortest_distances, shortest_paths] = all_pairs_shortest_paths(
			adjacency_matrix
		);

		// Reconstruct the shortest path
		let [ci, cj] = this.get_robot_position();
		let source_vertex = 0;
		let target_vertex = 0;
		let vertex_n = vertex_coordinates.length;
		for(let i = 0; i < vertex_n; i++) {
			let [vi, vj] = vertex_coordinates[i];
			if(vi == ci && vj == cj)
				source_vertex = i;
			if(vi == ti && vj == tj)
				target_vertex = i;
		}
		let path = [source_vertex];
		while(source_vertex != target_vertex) {
			source_vertex = shortest_paths[source_vertex][target_vertex];
			path = [...path, source_vertex];
		}

		// Reconstruct the shortest path directions
		let directions = []; // TODO: directions
		for(let i = 0; i < path.length-1; i++) {
			let v1 = path[i];
			let v2 = path[i+1];
			let c1 = vertex_coordinates[v1];
			let c2 = vertex_coordinates[v2];
			let direction = cardinal_relative_position(c1, c2);
			directions = [...directions, direction];
		}

		// Build the sequence of actions
		let actions = [];
		let orientation = this.get_robot_orientation();
		for(let direction of directions) {
			// If necessary push actions to correct the robot heading
			if(orientation != direction) {
				actions = [...actions, ...heading_actions(orientation, direction)];
				orientation = direction;
			}
			actions = [...actions, new Move()];
		}

		return actions;
	}

	copy() {
		return Board.from_json(JSON.parse(JSON.stringify(this)));
	}
}

Board.from_json = function(json) {
	let board = new Board();
	board.marker_n_at = json.marker_n_at;
	board.wall_at = json.wall_at;
	board.area_type_at = json.area_type_at;
	board.robot_marker_n = json.robot_marker_n;
	board.robot_position = json.robot_position;
	board.robot_orientation = json.robot_orientation;
	board.touched_is = json.touched_is;
	board.touched_js = json.touched_is;
	return board;
}

Board.random = function() {
	// TODO: make it actually random?
	let board = new Board();
	board.set_wall([1, 1], true);
	board.set_wall([0, 1], true);
	board.set_wall([-1, 1], true);
	board.set_wall([-1, 0], true);
	board.set_wall([-1, -1], true);
	board.set_wall([0, -1], true);
	board.set_wall([1, -1], true);
	board.put_marker_at([0, 0]);
	board.put_marker_at([2, 0]);
	board.put_marker_at([2, 0]);
	board.put_marker_at([2, 2]);
	board.set_area_type_at([0, 0], SPECIAL_AREA);
	board.set_area_type_at([1, 0], SPECIAL_AREA);
	board.set_robot_position([2, 0]);
	board.set_robot_orientation(UP);
	return board;
}

export {
	Board,
	RIGHT,
	LEFT,
	DOWN,
	UP,
	NORMAL_AREA,
	SPECIAL_AREA
};
