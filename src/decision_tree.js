import { If } from "./dsl/if.js";
import { IfElse } from "./dsl/ifelse.js";
import { Action } from "./dsl/action.js";
import { Condition } from "./dsl/condition.js";
import { random_item } from "./dsl/dsl.js";

function get_random_decision_tree(max_recursion) {
	if(max_recursion <= 0)
		return Action.random(max_recursion);
	let IF_CHOICE = 0;
	let IFELSE_CHOICE = 1;
	let choice = random_item([IF_CHOICE, IFELSE_CHOICE]);
	let condition = Condition.random(max_recursion);
	if(choice == IF_CHOICE) {
		// Ch
		let sub_tree = get_random_decision_tree(max_recursion-1);
		let tree = new If(condition, sub_tree);
		return tree;
	} else if(choice == IFELSE_CHOICE) {
		let sub_tree1 = get_random_decision_tree(max_recursion-1);
		let sub_tree2 = get_random_decision_tree(max_recursion-1);
		let tree = new IfElse(condition, sub_tree1, sub_tree2);
		return tree;
	}
}

export { get_random_decision_tree };
