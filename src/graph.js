/**
 * Algorithms on graph adjacency matrices.
 */

/**
 * Floyd-Warshall algorithm.
 *
 * Receives an adjacency matrix encoding
 * edge weights. Non-existent edges are expected to have the value Infinity.
 *
 * Returns two matrices: the first with all-pairs shortest
 * distances and the second with all-pairs shortest path
 * reconstruction data. Vertices in different components have a
 * distance of Infinity.
 *
 * Details:
 * https://en.wikipedia.org/wiki/Floyd%E2%80%93Warshall_algorithm
 */
function all_pairs_shortest_paths(adjacency_matrix) {
	let vertex_n = adjacency_matrix.length;

	// Initialize distances and paths
	let distance = Array(vertex_n).fill().map(() => Array(vertex_n).fill());
	let next = Array(vertex_n).fill().map(() => Array(vertex_n).fill());
	for(let i = 0; i < vertex_n; i++)
	for(let j = 0; j < vertex_n; j++) {
		distance[i][j] = adjacency_matrix[i][j];
		next[i][j] = null;
		if(i == j) {
			distance[i][i] = 0;
			next[i][i] = i;
		} else if(distance[i][j] < Infinity)
			next[i][j] = j;
	}

	for(let k = 0; k < vertex_n; k++)
	for(let i = 0; i < vertex_n; i++)
	for(let j = 0; j < vertex_n; j++) {
		let d_ik_kj = distance[i][k] + distance[k][j];
		if(distance[i][j] > d_ik_kj) {
			distance[i][j] = d_ik_kj;
			next[i][j] = next[i][k];
		}
	}

	return [distance, next];
}

export {
	all_pairs_shortest_paths
};
