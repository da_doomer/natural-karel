/**
 * Return [[tree_id, label_id], ...] of the closest utterances in the
 * entire library and the trees they label.
 */
function sorted_tree_label_ids(library, label, distance_function) {
	let tree_ids = library.get_tree_ids();
	if(tree_ids.length == 0)
		return [];

	// Get all [tree_id, label_id] pairs in the library
	let tree_label_ids = [];
	for(const tree_id of tree_ids)
	for(const label_id of library.get_label_ids_of_tree(tree_id))
		tree_label_ids = [...tree_label_ids, [tree_id, label_id]];

	// TODO: Sort the [tree_id, label_id] pairs by distance to given label
	function label_id_distance(tree_label_id1, tree_label_id2) {
		let tree_id1 = tree_label_id1[0];
		let tree_id2 = tree_label_id2[0];
		let label_id1 = tree_label_id1[1];
		let label_id2 = tree_label_id2[1];
		let label1 = library.get_tree_label(tree_id1, label_id1);
		let label2 = library.get_tree_label(tree_id2, label_id2);
		let label1_distance = distance_function(label, label1);
		let label2_distance = distance_function(label, label2);
		return label1_distance - label2_distance;
	}
	tree_label_ids.sort(label_id_distance);

	return tree_label_ids;
}

export {
	sorted_tree_label_ids
};
