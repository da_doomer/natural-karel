function hamming_distance(string1, string2) {
	let dist_counter = 0;
	for(let i = 0; i < string1.length; i++) {
		if(string1[i] != string2[i])
			dist_counter += 1;
	}
	return dist_counter;

}

export {
	hamming_distance
};
