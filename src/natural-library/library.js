/**
 * A natural library is a collection of trees from a DSL, each
 * labeled with one or more natural language strings.
 */


class NaturalLibrary {
	constructor() {
		this.labels = {};
		this.trees = {};
	}

	get_tree(tree_id) {
		return this.trees[tree_id];
	}

	get_tree_label(tree_id, label_id) {
		return this.labels[tree_id][label_id];
	}

	/**
	 * Return the list of tree IDs indexing the trees in the library.
	 */
	get_tree_ids() {
		let tree_ids = Object.keys(this.trees);
		return tree_ids;
	}

	_get_new_tree_id() {
		let tree_id = this.get_tree_ids().length;
		return tree_id;
	}

	_get_new_label_id(tree_id) {
		let label_id = Object.keys(this.labels[tree_id]).length;
		return label_id;
	}

	/**
	 * Return the natural language strings that label the tree
	 * indexed by the given ID.
	 */
	get_label_ids_of_tree(tree_id) {
		if(!(tree_id in this.labels))
			return [];
		return Object.keys(this.labels[tree_id]);
	}

	/**
	 * Add a natural language string to the tree indexed by the
	 * given ID.
	 *
	 * Returns a label ID.
	 */
	add_label_to_tree(tree_id, label) {
		if(!(tree_id in this.labels)) {
			this.labels[tree_id] = {};
		}
		let label_id = this._get_new_label_id(tree_id);
		this.labels[tree_id][label_id] = label;
		return label_id;
	}

	/**
	 * Add a tree to the library. An ID which indexes the tree
	 * in the library is returned.
	 */
	add_tree(tree) {
		let tree_id = this._get_new_tree_id();
		this.trees[tree_id] = tree;
		return tree_id;
	}
}


export {
	NaturalLibrary
};
