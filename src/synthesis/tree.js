import dt from "../../lib/decision-tree-js/decision-tree.js";
import { IfElse } from "../dsl/ifelse.js";

/*
 * Turn the board sequence into a list of feature-vectors
 * using the condition factories.
 *
 * The result is a vector for each board that is built by
 * testing each condition in the order in which their factories
 * are provided.
 */
function get_feature_vectors(board_sequence, condition_factories) {
	let conditions = [];
	for(const factory of condition_factories)
		conditions = [...conditions, new factory()];
	let feature_vectors = [];
	for(const board of board_sequence) {
		let features = [];
		for(const condition of conditions)
			features = [...features, condition.test(board)];
		feature_vectors = [...feature_vectors, features];
	}
	return feature_vectors;
}

/*
 * Turn the tree sequence into a list of integers representing
 * tree indexes (i.e. assign each tree in the tree sequence
 * an integer index).
 */
function get_tree_sequence_indexes(tree_sequence) {
	// We test for equality by converting to string.
	//
	// In general this is a necessary condition for equality (i.e.
	// tree equality implies string equality) but not sufficient
	// (i.e. string equality does not imply tree equality, for
	// example when two trees have different methods but equal
	// attributes).
	//
	// In our DSL this does not happen due to the type attribute and
	// therefore string equality this is both sufficient and
	// necessary.
	let indexer = {};
	let index = 0;
	for(const tree of tree_sequence) {
		let tree_id = JSON.stringify(tree);
		if(!(tree_id in indexer)) {
			indexer[tree_id] = index;
		}
		index = index + 1;
	}
	let indexes = [];
	for(const tree of tree_sequence) {
		let tree_id = JSON.stringify(tree);
		let index = indexer[tree_id];
		indexes = [...indexes, index];
	}
	return indexes;
}

/**
 * Use the feature-vectors and the integers as the inputs and
 * outputs for an unknown function that we wish to find (i.e.
 * solve a classification/regression problem).
 * Solve the classification problem with a decision-tree building
 * algorithm.
 */
function build_decision_tree(features, indexes ) {
	// Prepare target class and training feature names
	let features_n = features[0].length;
	let class_name = "y";

	// Prepare training dataset
	let training_dataset = [];
	for(let i = 0; i < features.length; i++) {
		let datum = {};
		for(let j = 0; j < features_n; j++)
			datum[j.toString()] = features[i][j];
		datum[class_name] = indexes[i].toString();
		training_dataset = [...training_dataset, datum];
	}

	// Create decision tree and train model
	let config = {
		trainingSet: training_dataset,
		categoryAttr: class_name
	};
	let decision_tree = new dt.DecisionTree(config);
	return decision_tree.root;
}

/**
 * Convert a tree back to a program in the DSL.
 */
function decision_tree_to_program(
		decision_tree,
		tree_sequence,
		condition_factories
	) {
	if("category" in decision_tree) {
		// case: the tree is a single leaf
		let tree_sequence_i = Number(decision_tree.category);
		return tree_sequence[tree_sequence_i];
	}

	// case: the are two children, a split variable and a split value
	let split_variable = decision_tree.attribute;
	let split_value = decision_tree.pivot;
	let condition_i = Number(split_variable);
	let condition = new condition_factories[condition_i]();
	let true_child = decision_tree.match;
	let false_child = decision_tree.notMatch;
	if(split_value == false) {
		// The algorithm may decide to split on false instead
		true_child = decision_tree.notMatch;
		false_child = decision_tree.match;
	}
	let true_tree = decision_tree_to_program(
		true_child,
		tree_sequence,
		condition_factories
	);
	let false_tree = decision_tree_to_program(
		false_child,
		tree_sequence,
		condition_factories
	);
	let tree = new IfElse(condition, true_tree, false_tree);
	return tree;
}

/*
 * Return a tree of conditions (for the inner nodes) and
 * sub-trees (for the leafs).
 */
function synthesize_tree(
		tree_sequence,
		board_sequence,
		condition_factories,
		) {
	let features = get_feature_vectors(board_sequence, condition_factories);
	let indexes = get_tree_sequence_indexes(tree_sequence);
	let decision_tree = build_decision_tree(features, indexes);
	let tree = decision_tree_to_program(
		decision_tree,
		tree_sequence,
		condition_factories
	);
	return tree;
}

export {
	synthesize_tree
};
